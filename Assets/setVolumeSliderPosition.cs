﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class setVolumeSliderPosition : MonoBehaviour {

	// Use this for initialization
	void Start () {

        if(PlayerPrefs.HasKey("music_volume"))
        {
            GetComponent<Slider>().value = PlayerPrefs.GetFloat("music_volume");
            GameObject.Find("SoundManager").GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("music_volume");
        }
        else
        {
            GetComponent<Slider>().value = GameObject.Find("SoundManager").GetComponent<AudioSource>().volume;
        }
        
	}
	
}
