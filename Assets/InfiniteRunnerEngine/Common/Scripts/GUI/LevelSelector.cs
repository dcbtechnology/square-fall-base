﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.InfiniteRunnerEngine
{	
	/// <summary>
	/// Add this component to a button so it can be used to go to a level, or restart the current one
	/// </summary>
	public class LevelSelector : MonoBehaviour
	{
	    public string LevelName;

		/// <summary>
		/// Asks the LevelManager to go to a specified level
		/// </summary>
	    public virtual void GoToLevel()
	    {
            Scene currentScene = SceneManager.GetActiveScene();

            // If leaving a playable level, save the game's state
            if(currentScene.name == "Minimal2D")
            {
                GameManager.Instance.SaveCurrentState();
            }

	        LevelManager.Instance.GotoLevel(LevelName);
	    }

		/// <summary>
		/// Restarts the current level.
		/// </summary>
	    public virtual void RestartLevel()
	    {
            //GameManager.Instance.UnPause();
            PlayerPrefs.SetInt("current_phase", 1);
            PlayerPrefs.SetFloat("speed", LevelManager.Instance.InitialSpeed);
            PlayerPrefs.SetFloat("current_score", 0);
            PlayerPrefs.SetInt("current_lives", 3);
            PlayerPrefs.SetInt("available_video_lives", 1);
            LevelManager.Instance.GotoLevel("Minimal2D");
	    }

        /// <summary>
		/// Restarts the current level.
		/// </summary>
	    public virtual void ContinueLevel()
        {
            //GameManager.Instance.UnPause();
            // GameObject.Find("PhaseManager").GetComponent<PhaseManager>().setIsContinue(true);
            LevelManager.Instance.GotoLevel("Minimal2D");
        }

        /// <summary>
        /// Resumes the game
        /// </summary>
        public virtual void Resume()
	    {
	        GameManager.Instance.UnPause();
	    }

	    /// <summary>
	    /// Resets the score.
	    /// </summary>
	    public virtual void ResetScore()
	    {
	    	SingleHighScoreManager.ResetHighScore();
	    }

        public void exitGame()
        {
            Application.Quit();
        }
	}
}
