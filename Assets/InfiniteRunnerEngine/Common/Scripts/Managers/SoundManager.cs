﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace MoreMountains.InfiniteRunnerEngine
{	
	/// <summary>
	/// This persistent singleton handles sound playing
	/// </summary>

	public class SoundManager : Singleton<SoundManager>
	{	
		/// true if the music is enabled	
		public bool MusicOn=true;
		/// true if the sound fx are enabled
		public bool SfxOn=true;
		/// the music volume
		[Range(0,1)]
		public float MusicVolume=0.3f;
		/// the sound fx volume
		[Range(0,1)]
		public float SfxVolume=1f;

        [Space(10)]
        [Header("Sounds")]
        public AudioClip jumpSound;
        public AudioClip dieSound;
        public AudioClip portalSound;
        public AudioClip pickupSound;
        public AudioClip gameOverSound;
        public AudioClip gravitySound;

        protected AudioSource _backgroundMusic; /// Unused background music variable
        public AudioSource bgMusic;
        public Slider musicVolumeSlider;

        /// <summary>
        /// Plays a background music.
        /// Only one background music can be active at a time.
        /// </summary>
        /// <param name="Clip">Your audio clip.</param>
        public virtual void PlayBackgroundMusic(AudioSource Music)
		{
			// if the music's been turned off, we do nothing and exit
			if (!MusicOn)
				return;
			// if we already had a background music playing, we stop it
			if (_backgroundMusic!=null)
				_backgroundMusic.Stop();
			// we set the background music clip
			_backgroundMusic=Music;
			// we set the music's volume
			_backgroundMusic.volume=MusicVolume;
			// we set the loop setting to true, the music will loop forever
			_backgroundMusic.loop=true;
            // we start playing the background music
            _backgroundMusic.Play();		
		}	

		public virtual AudioSource GetBackgroundMusic()
		{
			return _backgroundMusic;
		}
		
		/// <summary>
		/// Plays a sound
		/// </summary>
		/// <returns>An audiosource</returns>
		/// <param name="Sfx">The sound clip you want to play.</param>
		/// <param name="Location">The location of the sound.</param>
		/// <param name="Volume">The volume of the sound.</param>
		public virtual AudioSource PlaySound(AudioClip Sfx, Vector3 Location)
		{
			if (!SfxOn)
				return null;
			// we create a temporary game object to host our audio source
			GameObject temporaryAudioHost = new GameObject("TempAudio");
			// we set the temp audio's position
			temporaryAudioHost.transform.position = Location;
			// we add an audio source to that host
			AudioSource audioSource = temporaryAudioHost.AddComponent<AudioSource>() as AudioSource; 
			// we set that audio source clip to the one in paramaters
			audioSource.clip = Sfx; 
			// we set the audio source volume to the one in parameters
			audioSource.volume = SfxVolume;
			// we start playing the sound
			audioSource.Play(); 
			// we destroy the host after the clip has played
			Destroy(temporaryAudioHost, Sfx.length);
			// we return the audiosource reference
			return audioSource;
		}

        public void PlayRemoteSound(string name, Vector3 Location)
        {
            if (name == "Jump")
                PlaySound(jumpSound, Location);
            if (name == "Die")
                PlaySound(dieSound, Location);
            if (name == "Pickup")
                PlaySound(pickupSound, Location);
            if (name == "Portal")
                PlaySound(portalSound, Location);
            if (name == "Game Over")
                PlaySound(gameOverSound, Location);
            if (name == "Gravity")
                PlaySound(gravitySound, Location);
        }

        public AudioClip[] bgSongs;
        private int bgSongIndex;
        void Start()
        {
            bgMusic = GetComponent<AudioSource>();
            // bgMusic = true;
            if (PlayerPrefs.HasKey("music_volume") && bgMusic != null)
            {
                bgMusic.volume = PlayerPrefs.GetFloat("music_volume");
            }
            StartCoroutine(playBgSound());
        }

        IEnumerator playBgSound()
        {
            if(SceneManager.GetActiveScene().name == "CongratsScreen" || bgSongs == null)
            {
                yield break;
            }
            bgMusic.clip = getRandomBgMusic();
            bgMusic.Play();
            yield return new WaitForSeconds(bgMusic.clip.length);
            playBgSound();
        }

        private AudioClip getRandomBgMusic()
        {
            int randKey;

            // Get random song that is not the same as the current song
            do
            {
                randKey = Random.Range(0, bgSongs.Length);
            } while (randKey == bgSongIndex);

            bgSongIndex = randKey; // update song index

            return bgSongs[randKey];
        }

        public void PauseBackgroundMusic()
        {
            if (bgMusic != null)
            {
                bgMusic.Pause();
            }
        }

        public void UnPauseBackgroundMusic()
        {
            if (bgMusic != null)
            {
                bgMusic.UnPause();
            }
        }

        public void updateMusicVolumeFromSlider()
        {
            bgMusic.volume = musicVolumeSlider.value;
            PlayerPrefs.SetFloat("music_volume", musicVolumeSlider.value);
        }

    }
}