using UnityEngine;

namespace MoreMountains.Tools
{
	/// <summary>
	/// Singleton pattern.
	/// </summary>
	public class Singleton<T> : MonoBehaviour	where T : Component
	{
		protected static T _instance;
        protected static bool applicationIsQuitting = false;

        /// <summary>
        /// Singleton design pattern
        /// </summary>
        /// <value>The instance.</value>
        public static T Instance
		{
			get
			{
                if (applicationIsQuitting)
                {
                    return null;
                }

                if (_instance == null)
				{
					_instance = FindObjectOfType<T> ();
					if (_instance == null)
					{
						GameObject obj = new GameObject ();
						//obj.hideFlags = HideFlags.HideAndDontSave;
						_instance = obj.AddComponent<T> ();
					}
				}
				return _instance;
			}
		}

	    /// <summary>
	    /// On awake, we initialize our instance. Make sure to call base.Awake() in override if you need awake.
	    /// </summary>
	    protected virtual void Awake ()
		{
            applicationIsQuitting = false;
            _instance = this as T;			
		}

        public void OnDestroy()
        {
            // Debug.Log("Gets destroyed");
            applicationIsQuitting = true;
        }
    }
}
