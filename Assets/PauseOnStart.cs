﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.InfiniteRunnerEngine;

public class PauseOnStart : MonoBehaviour {

	// Use this for initialization
	void Awake () {
        if(GameManager.Instance)
        {
            GameManager.Instance.Pause();
        }
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
