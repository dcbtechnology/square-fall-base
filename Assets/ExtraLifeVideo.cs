﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using MoreMountains.InfiniteRunnerEngine;

public class ExtraLifeVideo : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    public void ShowVideoForLife()
    {
        ShowRewardedAd();
    }

    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                GameManager.Instance.SaveCurrentState();
                PlayerPrefs.SetInt("current_lives", 1);
                PlayerPrefs.SetInt("available_video_lives", PlayerPrefs.GetInt("available_video_lives") - 1);
                LevelManager.Instance.GotoLevel("Minimal2D");
                break;
            case ShowResult.Skipped:
                LevelManager.Instance.GotoLevel("MainScreen");
                Debug.Log("Skipped Video");
                break;
            case ShowResult.Failed:
                LevelManager.Instance.GotoLevel("MainScreen");
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
}
