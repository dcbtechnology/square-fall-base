﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.InfiniteRunnerEngine;
using MoreMountains.Tools;


public class GravityController : MonoBehaviour, MMEventListener<MMGameEvent>
{

    private Rigidbody2D rb;
    private Jumper jump;
    private PlayableCharacter player;

    // Use this for initialization
    void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        jump = gameObject.GetComponent<Jumper>();
        player = gameObject.GetComponent<PlayableCharacter>();

    }

    // Update is called once per frame
    void Update() {
        
    }

    void ReverseGravity()
    {
        rb.gravityScale = rb.gravityScale * -1;
        if(jump) jump.JumpForce = jump.JumpForce * -1;
        if (player)
        {
            player.gravityUp = !player.gravityUp;

            // Play sound
            SoundManager.Instance.PlayRemoteSound("Gravity", transform.position);
        }
    }
    
    protected virtual void OnEnable()
    {
        this.MMEventStartListening<MMGameEvent>();
    }

    protected virtual void OnDisable()
    {
        this.MMEventStopListening<MMGameEvent>();
    }

    public virtual void OnMMEvent(MMGameEvent gameEvent)
    {
        if(gameEvent.EventName == "Reverse Gravity")
        {
            ReverseGravity();
        }
        else if(gameEvent.EventName == "Gravity Up")
        {
            if(rb.gravityScale > 0)
            {
                ReverseGravity();
            }
            
        }
        else if (gameEvent.EventName == "Gravity Down")
        {
            if (rb.gravityScale < 0)
            {
                ReverseGravity();
            }
        }
    }
}
