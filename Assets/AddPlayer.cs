﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.InfiniteRunnerEngine;

public class AddPlayer : MonoBehaviour {

    private LevelManager level;
    private bool isTriggered = false;

    // Use this for initialization
    void Start () {
        level = LevelManager.Instance;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnEnable()
    {
        isTriggered = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isTriggered)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                AddToPlayer();
                isTriggered = true;
            }
        }
        
    }

    public void AddToPlayer()
    {
        PlayableCharacter currentLeadPlayer = level.CurrentPlayableCharacters[0];
        PlayableCharacter newCharacter = Instantiate(currentLeadPlayer);
        level.PlayableCharacters.Add(newCharacter);

        newCharacter.transform.position = new Vector3(currentLeadPlayer.transform.position.x - level.DistanceBetweenCharacters * level.CurrentPlayableCharacters.Count, currentLeadPlayer.transform.position.y, currentLeadPlayer.transform.position.z);
        // we set manually its initial position
        newCharacter.SetInitialPosition(newCharacter.transform.position);
        // we feed it to the game manager
        level.CurrentPlayableCharacters.Add(newCharacter);
        newCharacter.setIsMainPlayer(false);

        gameObject.SetActive(false);

        // Play sound
        SoundManager.Instance.PlayRemoteSound("Pickup", transform.position);
    }
    
}
