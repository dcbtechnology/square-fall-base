﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSpawner : MonoBehaviour
{

    private PhaseManager phase;
    private GameObject star1;
    private GameObject star2;
    private GameObject star3;

    // Use this for initialization
    void Awake()
    {

        // Collect necessary objects for later
        phase = GameObject.Find("PhaseManager").GetComponent<PhaseManager>();
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            GameObject obj = gameObject.transform.GetChild(i).gameObject;
            if (obj.name == "Star1")
                star1 = obj;
            if (obj.name == "Star2")
                star2 = obj;
            if (obj.name == "Star3")
                star3 = obj;
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        // Turn off all stars
        star1.SetActive(false);
        star2.SetActive(false);
        star3.SetActive(false);

        // Calculate max stars and choose number to generate
        int maxStars = (int)(phase.currentPhase / 3);
        if (maxStars > 3) maxStars = 3;
        int activeStars = Random.Range(0, maxStars);

        // Set star positions
        float self_width = gameObject.GetComponent<Collider2D>().bounds.size.x;
        star1.transform.localPosition += new Vector3(Random.Range(self_width / 2 * -1, self_width / 2), 0, 0);
        star2.transform.localPosition += new Vector3(Random.Range(self_width / 2 * -1, self_width / 2), 0, 0);
        star3.transform.localPosition += new Vector3(Random.Range(self_width / 2 * -1, self_width / 2), 0, 0);

        // Turn on appropriate number of stars
        if (activeStars > 0) star1.SetActive(true);
        if (activeStars > 1) star2.SetActive(true);
        if (activeStars > 2) star3.SetActive(true);

    }
}
