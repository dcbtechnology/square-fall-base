﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceScaleOnEnable : MonoBehaviour
{

    public float scaleValue = 1;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        Transform parentTransform = gameObject.transform.parent.transform;
        gameObject.transform.localScale = new Vector3(scaleValue / parentTransform.lossyScale.x, scaleValue / parentTransform.lossyScale.y, scaleValue / parentTransform.lossyScale.z);
    }
}
