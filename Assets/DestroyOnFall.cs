﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnFall : MonoBehaviour {

    public float yMax;
    public float yMin;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position.y > yMax || transform.position.y < yMin)
        {
            Destroy(gameObject);
        }
	}
}
