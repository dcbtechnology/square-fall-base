﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachToPlatform : MonoBehaviour
{
    private bool is_attached = false;
    public float self_height = 0;
    public float self_width = 0;

    // Use this for initialization
    void OnEnable()
    {
        is_attached = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!is_attached)
        {
            GameObject obj = FindClosestPlatform();

            if(obj)
            {
                self_width = gameObject.GetComponent<Collider2D>().bounds.size.x;
                self_height = gameObject.GetComponent<Collider2D>().bounds.size.y;

                float new_x = 0;
                float new_y = 0;

                int rand = Random.Range(0, 2);
                if (rand == 0)
                {
                    new_y = obj.transform.position.y + (obj.GetComponent<Renderer>().bounds.size.y / 2) + (self_height / 2);
                    gameObject.transform.eulerAngles = new Vector3(0, 0, 180);
                }
                else
                {
                    new_y = obj.transform.position.y - (obj.GetComponent<Renderer>().bounds.size.y / 2) - (self_height / 2);
                    gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
                }

                float min_x = obj.transform.position.x - (obj.GetComponent<Renderer>().bounds.size.x / 2) - (self_width / 2);
                float max_x = obj.transform.position.x + (obj.GetComponent<Renderer>().bounds.size.x / 2) + (self_width / 2);
                new_x = Random.Range(min_x, max_x);

                gameObject.transform.position = new Vector3(new_x, new_y, gameObject.transform.position.z);
                is_attached = true;
            }
            
        }
    }

    public GameObject FindClosestPlatform()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("Platform");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }
}
