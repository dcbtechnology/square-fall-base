﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.InfiniteRunnerEngine;
using MoreMountains.Tools;

public class PhaseManager : MonoBehaviour
{

    public double totalTime = 0;
    public int phaseLength;
    public int currentPhase = 1;
    static public bool isContinue = false;

    public GameObject PlatformSpawner;
    public GameObject SpikeSpawner;
    
    public Light MainLight;
    public Light BackgroundLight;
    public float colorFadeDuration = 1;

    private float colorChangeTime = 0;
    private Color mainColorCurrent;
    private Color bgColorCurrent;
    private Color mainColorNew;
    private Color bgColorNew;

    // Use this for initialization
    void Start()
    {
        if (PlayerPrefs.HasKey("current_phase") && PlayerPrefs.GetInt("current_phase") > 1)
        {
            ChangeToPhase(PlayerPrefs.GetInt("current_phase"));
            LevelManager.Instance.SetSpeed(PlayerPrefs.GetFloat("speed"));
            GameManager.Instance.SetPoints(PlayerPrefs.GetFloat("current_score"));
        }
            
        else
            ChangeToPhase(1);

        if(currentPhase > 1)
        { 
            LevelManager.Instance.InstructionsText = "";
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.Status == GameManager.GameStatus.GameInProgress)
        {
            totalTime += Time.deltaTime;
        }

        if(totalTime / phaseLength >= currentPhase + 1)
        {
            ChangeToPhase(currentPhase + 1);
        }

        if(totalTime >= LevelManager.Instance.gameTimeout && GameManager.Instance.Status == GameManager.GameStatus.GameInProgress)
        {
            // End current game
            LevelManager.Instance.finishObject.SetActive(true);
            
        }

        // Updating colors
        if(colorChangeTime <= 1)
        {
            colorChangeTime += Time.deltaTime / colorFadeDuration;
            if (mainColorCurrent != null && mainColorNew != null && mainColorCurrent != mainColorNew)
            {
                MainLight.color = Color.Lerp(mainColorCurrent, mainColorNew, colorChangeTime);
            }
            if (bgColorCurrent != null && bgColorNew != null && bgColorCurrent != bgColorNew)
            {
                BackgroundLight.color = Color.Lerp(bgColorCurrent, bgColorNew, colorChangeTime);
            }
        }
        else
        {
            bgColorCurrent = bgColorNew;
            mainColorCurrent = mainColorNew;
        }
        
    }

    public void ChangeToPhase(int phaseNum)
    {
        currentPhase = phaseNum;

        // Set platform length
        // Set platform gap
        if (PlatformSpawner != null)
        {
            float maxGap = 5;
            if (phaseNum > 15) maxGap = 8;
            else if (phaseNum > 10) maxGap = 7;
            else if (phaseNum > 5) maxGap = 6;
            PlatformSpawner.GetComponent<DistanceSpawner>().MaximumGap = new Vector3(maxGap, 4, 0);
        }

        // Set number of spikes
        if (SpikeSpawner != null)
        {
            SpikeSpawner.GetComponent<TimedSpawner>().MinSpawnTime = (float)(8 / (Mathf.Log(phaseNum) + 0.5));
            SpikeSpawner.GetComponent<TimedSpawner>().MaxSpawnTime = (float)(15 / (Mathf.Log(phaseNum) + 0.5));
        }
        

        // Calculate colors
        UpdateColors();
        
    }

    private void UpdateColors()
    {
        Color bgColor = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        float h, s, v;
        Color.RGBToHSV(bgColor, out h, out s, out v);
        Color mainColor = Color.HSVToRGB((h - 0.1f), s, (v + 0.05f));

        bgColorNew = bgColor;
        mainColorNew = mainColor;

        if(bgColorCurrent == null)
        {
            bgColorCurrent = bgColor;
        }
        if(mainColorCurrent == null)
        {
            mainColorCurrent = mainColor;
        }

        colorChangeTime = 0;
    }

    public void setIsContinue(bool newIsContinue)
    {
        isContinue = newIsContinue;
    }
}
