﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.InfiniteRunnerEngine;
using MoreMountains.Tools;

public class GravityFlip : MonoBehaviour, MMEventListener<MMGameEvent>
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    protected virtual void OnEnable()
    {
        this.MMEventStartListening<MMGameEvent>();
    }

    protected virtual void OnDisable()
    {
        this.MMEventStopListening<MMGameEvent>();
    }

    public virtual void OnMMEvent(MMGameEvent gameEvent)
    {
        if (gameEvent.EventName == "Reverse Gravity")
        {
            FlipArrow();
        }
        else if (gameEvent.EventName == "Gravity Up")
        {
            FlipArrowUp();
        }
        else if (gameEvent.EventName == "Gravity Down" || gameEvent.EventName == "LifeLost")
        {
            FlipArrowDown();
        }
    }

    private void FlipArrow()
    {
        gameObject.transform.eulerAngles = new Vector3(0, 0, gameObject.transform.eulerAngles.z + 180);
    }

    private void FlipArrowUp()
    {
        gameObject.transform.eulerAngles = new Vector3(0, 0, 180);
    }

    private void FlipArrowDown()
    {
        gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
    }
}
