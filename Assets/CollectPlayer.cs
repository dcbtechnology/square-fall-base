﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.InfiniteRunnerEngine;

public class CollectPlayer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.GetComponent<PlayableCharacter>().isMainPlayer == false)
            {
                GameManager.Instance.AddPoints(100);
                LevelManager.Instance.CurrentPlayableCharacters.Remove(collision.gameObject.GetComponent<PlayableCharacter>());
                LevelManager.Instance.PlayableCharacters.Remove(collision.gameObject.GetComponent<PlayableCharacter>());
                Destroy(collision.gameObject);

                // Play sound
                SoundManager.Instance.PlayRemoteSound("Portal", transform.position);
            }
        }

    }
}
